﻿using System;

namespace LV1CSHARP
{
    class Program
    {
        static void Main(string[] args)
        {
            Note zabiljeska1, zabiljeska2, zabiljeska3;
            zabiljeska1 = new Note();
            zabiljeska2 = new Note("Nemoj zaboravit uploadat na git","Krunoslav Petrik", 5);
            zabiljeska3 = new Note("Zelim neoznacene novcanice", 10);
            Console.WriteLine("Prva zabiljeska: <" + zabiljeska1.getText() + "> Autor biljeske je: " + zabiljeska1.getAuthor());
            Console.WriteLine("Druga zabiljeska: <" + zabiljeska2.getText() + "> Autor biljeske je: " + zabiljeska2.getAuthor());
            Console.WriteLine("Treca zabiljeska: <" + zabiljeska3.getText() + "> Autor biljeske je: " + zabiljeska3.getAuthor());
            Console.WriteLine("Treca: " + zabiljeska3.ToString());

            TimeNote vrijemeZab;
            vrijemeZab = new TimeNote();
            Console.WriteLine(vrijemeZab);
        }
    }
}
