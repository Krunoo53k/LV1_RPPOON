﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV1CSHARP
{
    class TimeNote : Note
    {
        private DateTime creationTime;

        public TimeNote():base()
        {
            creationTime = DateTime.Now;
        }

        public TimeNote(DateTime creationTime, string text, string author, int priorityLevel) : base( text,  author,  priorityLevel)                                    
        {
            this.creationTime =creationTime;
        }

        public DateTime CreationTime { get => creationTime; set => creationTime = value; }

        public override string ToString()
        {
            return base.ToString()+" Ova poruka je izgenerirana "+creationTime;
        }
    }
}

