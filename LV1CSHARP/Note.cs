﻿using System;

class Note
{
	private String text;
	private String author;
	private int priorityLevel;
	private bool canChangeAuthor;



	public Note()
	{
		this.text = "N/A";
		this.author = "N/A";
		this.priorityLevel = 0;
		this.canChangeAuthor = true;
	}
	public Note(String text, int priorityLevel)
	{
		this.text = text;
		this.author = "N/A";
		this.priorityLevel = priorityLevel;
		this.canChangeAuthor = true;
	}
	public Note(String text, String author, int priorityLevel)
	{
		this.text = text;
		this.author = author;
		this.priorityLevel = priorityLevel;
		this.canChangeAuthor = false;
	}

	public string getText() { return this.text; }
	public string getAuthor() { return this.author; }
	public int getPriorityLevel() { return this.priorityLevel; }

	public void setText(string text) { this.text = text; }
	public void setAuthor(string author)
	{
		if (canChangeAuthor)
		{
			this.author = author;
			canChangeAuthor = false;
		}
		else
			Console.WriteLine("Nije moguce promijeniti ime autora zabiljeske.");
	}
	public void setPriorityLevel(int priorityLevel) { this.priorityLevel = priorityLevel; }

	public string Text { get => text; set => text = value; }
	public string Author
	{
		get => author;
		set
		{
			if (canChangeAuthor)
				author = value;
			else
				Console.WriteLine("Nije moguce promijeniti ime autora zabiljeske.");
		}
	}
	public int PriorityLevel { get => priorityLevel; set => priorityLevel = value; }

	public override string ToString()
	{
		return "Text biljeske je: <<" + this.text + ">> Autor biljeske je: " + this.author + ". Prioritet zabiljeske je: " + this.priorityLevel + ".";
	}
}